package com.example.EmailSender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@RestController
public class emailSenderController {

    private emailSenderService mailService;

    @Autowired
    public emailSenderController(emailSenderService mailService) {
        this.mailService = mailService;
    }

    @GetMapping("/sendMail")
    public String sendMail() throws MessagingException {
        mailService.sendMail("anitakozon6@gmail.com",
                "Anitaaaa",
                "<b>Dżem Dobry Anitita to ja Twój Kubiteł</b><br>:P", true);
        return "wysłano";
    }
}
